MKL=/opt/intel/oneapi/mkl/latest
all: dgesv

dgesv: dgesv.c
	gcc -I/opt/intel/oneapi/mkl/latest/include -L/opt/intel/oneapi/mkl/latest/lib/64 -lm -fopenmp -o dgesv dgesv.c

debug: dgesv.c
	gcc -g -O3 -fno-asm -I/opt/intel/oneapi/mkl/latest/include -L/opt/intel/oneapi/mkl/latest/lib/64 -lm -fopenmp -o dgesv dgesv.c

perf: dgesv.c
	gcc -O3 -I/opt/intel/oneapi/mkl/latest/include -L/opt/intel/oneapi/mkl/latest/lib/64 -lm -fopenmp -o dgesv dgesv.c

run:
	echo "Small test"
	./dgesv 2048
	echo "Medium test"
	./dgesv 4096
	echo "Large test"
	./dgesv 8192

clean:
	rm dgesv

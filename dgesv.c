#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "mkl_lapacke.h"
#include <omp.h>

double *generate_matrix(int size)
{
    int i;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    srand(1);

    for (i = 0; i < size * size; i++)
    {
        matrix[i] = rand() % 100;
    }

    return matrix;
}

void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("matrix: %s \n", name);

    for (i = 0; i < size; i++)
    {
            for (j = 0; j < size; j++)
            {
                printf("%f ", matrix[i * size + j]);
            }
            printf("\n");
    }
}

int check_result(double *bref, double *b, int size) {
    int i;
    double epsilon = 0.1;
    for(i=0;i<size*size;i++) {
        if(abs(b[i]) > epsilon && abs(b[i]-1) > epsilon){
            printf("%f\n", b[i]);
            return 0;
        }
        /*
        if ( abs(bref[i]-b[i]) > epsilon ){
            printf("%f != %f\n", bref[i], b[i]);
            return 0;
        }
        */
    }
    return 1;
}

int my_dgesv(int n, int nrhs, double *a, int lda, int *ipiv, double *b, int ldb) {
	/*
    https://www.gaussianwaves.com/2013/05/solving-a-triangular-matrix-using-forward-backward-substitution/
    http://www.netlib.org/lapack/explore-html/d7/d3b/group__double_g_esolve_ga5ee879032a8365897c3ba91e3dc8d512.html
    */
    
    int i, j, k;
    double sum;
    double *y = (double *)malloc(sizeof(double) * n * n);
    //print_matrix("A", a, n);
    //print_matrix("B", b, n);

    // LU
    for(i=0; i<n; i++){
        for(j=0; j<i; j++){
            sum = a[i * n + j];
            for(k=0; k<j; k++){
                sum -= a[i * n + k] * a[k * n + j];
            }
            a[i * n + j] = sum / a[j * n + j];
        }
        for(j=i; j<n; j++){
            sum = a[i * n + j];
            for(k=0; k<i; k++)
                sum -= a[i * n + k] * a[k * n + j];
            a[i * n + j] = sum;
        }
    }
    //print_matrix("LU", a, n);

    // Forward
    #pragma omp parallel shared(a, b, y, n) private(i, j, k, sum)
    {
    #pragma omp for simd aligned(y, b: 64)
    #pragma ivdep
    for(i=0; i<n*n; i++)
        y[i] = b[i];
    
    
    for(i=0; i<n; i++){
        for(j=0; j<i; j++){
            #pragma omp for simd aligned(y, a: 64)
            #pragma ivdep
            for(k=0; k<n; k++){
                y[i * n + k] -= a[i * n + j] * y[j * n + k];
            }
        }
    }
    //print_matrix("Forward", y, n);

    // Backward
    #pragma omp for simd
    for(i=(n-1); i>-1; i--)
        for(j=(i+1); j<n; j++){
            a[i*n+j] /= a[i*n+i];
        }

    #pragma omp for simd aligned(y, a: 64)
    #pragma ivdep
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            y[i*n+j] /= a[i*n+i];

    for(i=(n-1); i>-1; i--){
        for(j=(i+1); j<n; j++){
            #pragma omp for simd aligned(y, a, b: 64)
            #pragma ivdep
            for(k=0; k<n; k++){
                y[i * n + k] -= a[i * n + j] * b[j * n + k];
            }    
        }
        #pragma omp for simd aligned(y, b: 64)
        #pragma ivdep
        for(k=0; k<n; k++){
            b[i * n + k] = y[i * n + k];
        }
    }
    
    //print_matrix("Backward", b, n);
    }

    free(y);

}


void main(int argc, char *argv[])
{

    int size = atoi(argv[1]);

    double *a, *aref;
    double *b, *bref;
    double start;

    a = generate_matrix(size);
    aref = generate_matrix(size);        
    b = generate_matrix(size);
    bref = generate_matrix(size);
    

    //print_matrix("A", a, size);
    //print_matrix("B", b, size);

    // Using MKL to solve the system
    MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
    MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

    //info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
    //printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    start = omp_get_wtime(); 
    MKL_INT *ipiv2 = (MKL_INT *)malloc(sizeof(MKL_INT)*size);        
    my_dgesv(n, nrhs, a, lda, ipiv2, b, ldb);
    printf("Time taken by my implementation: %.2fs\n", omp_get_wtime() - start);
    
    if (check_result(bref,b,size)==1)
        printf("Result is ok!\n");
    else    
        printf("Result is wrong!\n");
    
    //print_matrix("X", b, size);
    //print_matrix("Xref", bref, size);

    free(a);
    free(aref);
    free(b);
    free(bref);


    free(ipiv);
    free(ipiv2);

}
